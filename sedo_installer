#!/usr/bin/env bash

# Author 	 -  sum4n
# Source 	 -  https://gitlab.com/sum4n/SEDO
# Maintainer -  to_suman@outlook.com

printf "Hello!! \n\n"
printf "This script will install needed dependencies for SEDO rice to your machine. \n\nThis installer script does not remove any of your config. \n\nIt move your all config file to %s/.sedo_bakup \n" "$HOME"
sleep 2

# ------------------------------------------- #
# ---- check user want to install [SEDO] ----
# ------------------------------------------- #
while true
do
  read -rp "Do you wish to continue? [y/N] " yn
  if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
    break
  elif [ "$yn" = "n" ] || [ "$yn" = "n" ]; then
    printf 'Ok bye!!'
    exit
  else
    printf 'Wrong: choose [y] or [n | N]'
    sleep 1
    # clear
  fi
done
# clear

# ----------------------------- #
# ---- check user's distro ----
# ----------------------------- #
sleep 2
while true
do
  read -rp "what is your distro [arch(a) OR debian(d)]? " distro
  if [ "$distro" = "debian" ] || [ "$distro" = "d" ]; then
    dependenci='mpv zsh hwinfo htop feh picom lightdm brightnessctl xautolock cron light bspwm file-roller mlocate pulsemixer rofi suckless-tools network-manager-gnome dunst policykit-1-gnome sxhkd nemo pcmanfm lxappearance qt5ct udiskie copyq fish nitrogen vim viewnior volumeicon-alsa ranger python3-pip npm mpd ncmpcpp mpc gparted pavucontrol kdeconnect ufw xclip maim git curl wget ripgrep polybar htop neofetch playerctl pcmanfm acpi gpick qt5-style-kvantum numlockx xfce4-settings i3lock-color xfce4-power-manager slop network-manager xorg xinit pulseaudio build-essential bluez blueman xdotool xdo autoconf exa lightdm-gtk-greeter lightdm-gtk-greeter-settings firefox-esr lf fd-find fzf alacritty cava socat jq kitty'
    if ! command -v nala > /dev/null 2>&1; then
      sudo apt install -y nala
    fi
    break
  elif [ "$distro" = "arch" ] || [ "$distro" = "a" ]; then
    dependenci='lightdm fzf fd feh zsh htop hwinfo mpv kvantum numlockx brightnessctl light xautolock file-roller mlocate rofi-emoji pulsemixer bspwm rofi dmenu network-manager-applet dunst polkit-gnome sxhkd alacritty nemo lxappearance qt5ct udiskie copyq fish nitrogen vim neovim viewnior volumeicon pacman-contrib ranger python-pip npm mpd ncmpcpp mpc gparted yarn pavucontrol kdeconnect ufw gufw xclip maim git curl wget lazygit python-pywal ripgrep polybar ntfs-3g playerctl btop neofetch android-tools android-file-transfer pcmanfm acpi cronie libxcrypt-compat gpick fortune-mod xfce4-settings xfce4-power-manager networkmanager archlinux-keyring xorg xorg-xinit pulseaudio pulseaudio-alsa xf86-video-amdgpu base-devel bluez bluez-utils blueman xdotool xdo autoconf slop exa firefox lightdm-gtk-greeter lightdm-gtk-greeter-settings papirus-icon-theme lf socat jq kitty'
    break
  else
    printf 'Wrong: choose distro [debian] or [arch]'
    sleep 1
    # clear
  fi
done
# clear

# ------------------------------------------------------- #
# ---- check distro and install respected dependenci ----
# ------------------------------------------------------- #
if [ "$distro" = "arch" ] || [ "$distro" = "a" ]; then
  is_installed() {
    pacman -Qi "$1" > /dev/null 2>&1
    return $?
  }
  
  for package in "${dependenci[@]}"
  do
    if ! is_installed "$package"; then
      sudo pacman -S "$package" --noconfirm
      printf "\n"
    else
      printf '%s is already installed on your system!\n' "$package"
    fi
  done
  sleep 2

# --------------------------------------- #
# ---- check yay is installed or not ----
# --------------------------------------- #
  printf "checking yay is installed. \n"
  if ! command -v yay &>/dev/null; then
    printf "Wrong: yay is not installed. \n"
    sleep 1
    printf "Loading yay on your system. \n"
    test ! -d "$HOME"/git_files && mkdir "$HOME"/git_files
    cd "$HOME"/git_files || return
		git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si
  else
    printf "yay is installed. \n"
  fi
  sleep 2

# ---------------------------------------------------- #
# ---- after check yay. install packages from aur ----
# ---------------------------------------------------- #
  if ! command -v i3lock &>/dev/null; then
    printf "Pull packages from AUR. \n"
    yay -S picom-git i3lock-color stacer-bin cava #--noconfirm --removemake
  else
    printf "aur packages already installed. \n"
  fi
  sleep 1

elif [[ $distro == "debian" || $distro == "d" ]]; then
  is_installed() {
    dpkg -s "$1" &> /dev/null
    return $?
  }

  if command -v nala &>/dev/null; then
    for package in "${dependenci[@]}"
    do
      if ! is_installed "$package"; then
        sudo nala install -y "$package" 
        printf "\n"
      else
        printf '%s is already installed on your system! \n' "$package"
      fi
    done
  else
    for package in "${dependenci[@]}"
    do
      if ! is_installed "$package"; then
        sudo apt install -y "$package"
        printf "\n"
      else
        printf '%s is already installed on your system!\n' "$package"
      fi
    done
  fi
  sleep 1
fi


# ---- # --------------------------- # ---- #
# ---- # ---- setting up [SEDO] ---- # ---- #
# ---- # --------------------------- # ---- #
# ---- final setup ----
# ---- clone my rice ----
# ----------------------- #
if test ! -e "$HOME"/SEDO; then
  printf "cloning the sedo rice. \n"
  cd "$HOME" && git clone https://gitlab.com/sum4n/SEDO.git
fi
sleep 1

# ---------------------------- #
# ---- creating bakup dir ----
# ---------------------------- #
printf "bakup file's will be stored in %s/.sedo_bakup \n" "$HOME"
test ! -d "$HOME"/.sedo_bakup && mkdir "$HOME"/.sedo_bakup
sleep 3

# variables
printf "executing final setup. \n"
SEDO="$HOME/SEDO"
CONFIG="$HOME/.config"
LOCAL="$HOME/.local"
BAKUP="$HOME/.sedo_bakup"

# --------------------------------- #
# ---- starship prompt install ----
# --------------------------------- #
if ! command -v starship &>/dev/null; then
  printf "StarShip prompt installing\n"
  curl -sS https://starship.rs/install.sh | sh
fi

# --------------------------------------------------- #
# ---- rofi emoji plugin install for debian only ----
# --------------------------------------------------- #
if test ! -e "$HOME"/git_files/rofi-emoji; then
  if [[ $distro == "debian" || $distro == "d" ]]; then
    sudo nala install rofi-dev autoconf automake libtool-bin libtool
    test ! -e "$HOME"/git_files && mkdir "$HOME"/git_files
    cd "$HOME"/git_files && git clone https://github.com/Mange/rofi-emoji.git
    cd rofi-emoji && autoreconf -i && mkdir build && cd build/ && ../configure && make && sudo make install
  fi
fi

# ------------------------ #
# ---- required dir's ----
# ------------------------ #
sleep 2
printf "creating required directory. \n"
test ! -d "$HOME"/.config && mkdir "$HOME"/.config
test ! -e /media/sDrive && sudo mkdir -p /media/sDrive
test ! -e "$HOME"/.local/bin && mkdir -p "$HOME"/.local/bin
test ! -e "$HOME"/.local/share/icons && mkdir -p "$HOME"/.local/share/icons
test ! -e "$LOCAL"/share/themes && mkdir "$LOCAL"/share/themes

# ------------------------------- #
# ---- clone my scripts repo ----
# ------------------------------- #
if test -e "$HOME"/bin; then
  mv "$HOME"/bin "$BAKUP"/bin.bak."$(date +%Y.%m.%d-%H.%M.%S)"
  git clone https://gitlab.com/sum4n/bin.git
else
  git clone https://gitlab.com/sum4n/bin.git
fi

# ---------------------------------------------------- #
# ---- backup old and insert [SEDO] rice dotfiles ----
# ---------------------------------------------------- #
sleep 1
while true
do
  read -rp 'Final alert to insert [SEDO] rice? (y/n): ' bakup
  if [[ $bakup == "y" || $bakup == "Y" ]]; then
    printf "Backup the respected directory && inserting mine. \n"
  # config dir files
    test -e "$CONFIG"/alacritty && mv "$CONFIG"/alacritty "$BAKUP"/alacritty.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/bspwm && mv "$CONFIG"/bspwm "$BAKUP"/bspwm.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/dunst && mv "$CONFIG"/dunst "$BAKUP"/dunst.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/fish && mv "$CONFIG"/fish "$BAKUP"/fish.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/kitty && mv "$CONFIG"/kitty "$BAKUP"/kitty.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/mpd && mv "$CONFIG"/mpd "$BAKUP"/mpd.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/ncmpcpp && mv "$CONFIG"/ncmpcpp "$BAKUP"/ncmpcpp.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/picom && mv "$CONFIG"/picom "$BAKUP"/picom.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/ranger && mv "$CONFIG"/ranger "$CONFIG"/ranger.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/rofi && mv "$CONFIG"/rofi "$BAKUP"/rofi.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$CONFIG"/sxhkd && mv "$CONFIG"/sxhkd "$BAKUP"/sxhkd.bak."$(date +%Y.%m.%d-%H.%M.%S)"
  
  # home dir files
    test -e "$HOME"/.bash_profile && mv "$HOME"/.bash_profile "$BAKUP"/.bash_profile.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$HOME"/.bashrc && mv "$HOME"/.bashrc "$BAKUP"/.bashrc.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$HOME"/.profile && mv "$HOME"/.profile "$BAKUP"/.profile.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$HOME"/.vimrc && mv "$HOME"/.vimrc "$BAKUP"/.vimrc.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$HOME"/.xinitrc && mv "$HOME"/.xinitrc "$BAKUP"/.xinitrc.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$HOME"/.Xresources && mv "$HOME"/.Xresources "$BAKUP"/.Xresources.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$HOME"/.Xmodmap && mv "$HOME"/.Xmodmap "$BAKUP"/.Xmodmap.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    test -e "$HOME"/.zshrc && mv "$HOME"/.zshrc "$BAKUP"/.zshrc.bak."$(date +%Y.%m.%d-%H.%M.%S)"

    break
  elif [[ $bakup == "n" ]]; then
    printf "terminate the script... \n"
    sleep 1
    exit
  else
    printf "WRONG: choose [y] or [n] \n"
  fi
done

# -------------------------- #
# ---- link all folders ----
# -------------------------- #
sleep 2
printf "link files & folders to respective dir's \n"
ln -s ~/SEDO/home/.* ~/
ln -s ~/SEDO/config/* ~/.config/

if test ! -e "$HOME"/.local/bin/i3lock-everblush; then
  ln -s "$HOME"/SEDO/scripts/* ~/.local/bin/
fi

test ! -e ~/.local/share/icons/dunst && ln -s ~/SEDO/local/icons/dunst ~/.local/share/icons/

if test -e "$LOCAL"/share/mpd; then
  mv "$LOCAL"/share/mpd "$BAKUP"/mpd.bak."$(date +%Y.%m.%d-%H.%M.%S)"
  cp -r "$SEDO"/local/mpd "$LOCAL"/share/mpd
else
  cp -r "$SEDO"/local/mpd "$LOCAL"/share/mpd
fi


# ----------------------- #
# ---- install fonts ----
# ----------------------- #
sleep 1
printf "Install and update fonts... \n"
test ! -e "$HOME"/.local/share/fonts && mkdir "$HOME"/.local/share/fonts
if test ! -e "$LOCAL"/share/fonts/sedo_font; then
  test ! -d "$HOME"/git_files && mkdir "$HOME"/git_files
  cd "$HOME"/git_files
  test ! -d sedo-fonts && git clone https://gitlab.com/sum4n/sedo-fonts.git
  cd sedo-fonts && tar -xvzf sedo_font.tar.gz && mv sedo_font "$LOCAL"/share/fonts/ && fc-cache -rv >/dev/null 2>&1
  printf "Fonts updated successfully\n"
else
  printf "Fonts are already here!!\n"
fi

# ------------------------------- #
# ---- everblush theme setup ----
# ------------------------------- #
sleep 2
test ! -d /usr/share/themes && sudo mkdir -p /usr/share/themes
if test ! -e /usr/share/themes/Everblush; then
  cd "$SEDO"/local && unzip Everblush-Theme.zip
  sudo mv Everblush /usr/share/themes
  sudo mv Everblush-xfwm /usr/share/themes
fi
sleep 1
test -d "$HOME"/.config/Kvantum && mv "$HOME"/.config/Kvantum "$BAKUP"/Kvantum.bak."$(date +%Y.%m.%d-%H.%M.%S)"
cd "$SEDO"/local && unzip Kvantum-theme.zip && mv Kvantum "$HOME"/.config

# ------------------------------ #
# ---- cursor pack download ----
# ------------------------------ #
test ! -e "$HOME"/git_files && mkdir ~/git_files
cd ~/git_files || return
test ! -e Radioactive-nord && git clone https://github.com/alvatip/Radioactive-nord.git
if test ! -e /usr/share/icons/Radioactive-nord; then
  cd Radioactive-nord/ && sudo cp -r Radioactive-nord /usr/share/icons/
fi

# ---------------------------- #
# ---- icon pack download ----
# ---------------------------- #
cd ~/git_files || return
test ! -d luv-icon-theme && git clone https://github.com/Nitrux/luv-icon-theme.git
if test ! -d /usr/share/icons/Luv; then
  cd luv-icon-theme && sudo mv Luv /usr/share/icons/
fi

# ------------------------------------ #
# ---- some daemon service enable ----
# ------------------------------------ #
sudo systemctl enable systemd-timesyncd.service --now
sudo systemctl enable ufw.service --now
sudo systemctl enable bluetooth
systemctl enable --user mpd.service
systemctl enable --user mpd.socket
sudo ufw enable

if [[ $distro == "debian" || $distro == "d" ]]; then
  sudo systemctl enable cron.service --now
else
  sudo systemctl enable cronie.service --now
fi

# ---------------------------------- #
# ---- ufw allow for kdeconnect ----
# ---------------------------------- #
sudo ufw allow 1714:1764/udp
sudo ufw allow 1714:1764/tcp
 
# --------------- #
# ---- fstab ----
# --------------- #
sleep 1
cd ~/SEDO/ || return
FSTAB=$(grep 72A0EB18A0EAE19 /etc/fstab | awk '{print $1}' | head -n1)
UUId="UUID=72A0EB18A0EAE199"
if [[ $FSTAB != "$UUId" ]]; then
  sudo sh -c 'cat fstab >> /etc/fstab'
fi

# ------------------------------- #
# ---- set snvironment=qt5ct ----
# ------------------------------- #
sleep 1
ENVIRONMENT=$(grep '^QT_QPA' /etc/environment)
if [[ $ENVIRONMENT != "QT_QPA_PLATFORMTHEME=qt5ct" ]]; then
  sudo sh -c 'printf "QT_QPA_PLATFORMTHEME=qt5ct\n" >> /etc/environment'
fi

# ------------------------------------- #
# ---- for librewolf/firefox setup ----
# ------------------------------------- #
sleep 1
while true
do
  read -rp 'firefox custom css input-(y/n)? ' fire
  if [[ $fire == "y" ]]; then
    cp -R "$SEDO"/chrome "$HOME"/.mozilla/firef*/*default-*/
    break
  elif [[ $fire == "n" ]]; then
    printf "next... \n"
    break
  else
    printf "WRONG: choose [y] or [n]..."
  fi
done

# ------------------------------------------------- #
# --- Nemo's default-terminal is now alacritty ----
# ------------------------------------------------- #
if command -v nemo &>/dev/null; then
  gsettings set org.cinnamon.desktop.default-applications.terminal exec alacritty
fi

# Copy neovim desktop to application folder
if test ! -d "$LOCAL"/share/applications; then
  mkdir "$LOCAL"/share/applications && cp -r "$HOME"/SEDO/local/nvim.desktop "$HOME"/.local/share/applications
else
  cp -r "$HOME"/SEDO/local/nvim.desktop "$HOME"/.local/share/applications
fi

# --------------------------------------- #
# ---- touchpad and gpu config setup ----
# --------------------------------------- #
sleep 1
XORG="/etc/X11/xorg.conf.d"
if [[ -e "$XORG/30-touchpad.conf" ]]; then
  sudo mv /etc/X11/xorg.conf.d/30-touchpad.conf /etc/X11/xorg.conf.d/30-touchpad.conf.bak."$(date +%Y.%m.%d-%H.%M.%S)"
  sudo cp -r "$HOME"/SEDO/30-touchpad.conf "$XORG"/
elif [[ ! -e "$XORG/30-touchpad.conf" ]]; then
    sudo cp -r "$HOME"/SEDO/30-touchpad.conf "$XORG"/
else
  printf "next... \n"
fi
GPU=$(light -L | grep 'backlight' | head -n1 | cut -d'/' -f3)
if [[ $GPU == "amdgpu_bl0" ]]; then
  if [[ -e "$XORG/20-amd-gpu.conf" ]]; then
    sudo mv $XORG/20-amd-gpu.conf $XORG/20-amd-gpu.conf.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    sudo cp -r "$HOME"/SEDO/20-amd-gpu.conf "$XORG"/
  elif [[ ! -e "$XORG/20-amd-gpu.conf" ]]; then
    sudo cp -r "$HOME"/SEDO/20-amd-gpu.conf "$XORG"/
  else
    printf "next... \n"
  fi
else
  printf "It's not an amdgpu. for gpu config \n"
fi

# -------------------------------------------------------- #
# ---- download >NeoVim[0.8.3] stable for debian only ----
# -------------------------------------------------------- #
if ! command -v nvim &>/dev/null; then
  if [[ $distro == "debian" || $distro == "d" ]]; then
    test ! -e "$HOME"/Downloads && mkdir "$HOME"/Downloads
    cd "$HOME"/Downloads && wget https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.deb
    sudo apt install ./nvim-linux64.deb && printf "NeoVim is installed now!!\n"
  fi
fi

# ---------------------------------------- #
# ---- pulling my NeoVim configuraion ----
# ---------------------------------------- #
sleep 1
while true
do
  read -rp 'You like to setup Neovim like mine? (y/n): ' nvim
  if [[ -e ~/".config/nvim" ]] && [[ $nvim == "y" ]]; then
    mv ~/.config/nvim "$BAKUP"/nvim.bak."$(date +%Y.%m.%d-%H.%M.%S)"
    printf "Downloadng the NeoVim conf from gitlab. \n"
    cd ~/.config || return
    git clone https://gitlab.com/sum4n/nvim.git
    break
  elif [[ ! -e ~/".config/nvim" ]] && [[ $nvim == "y" ]]; then
    printf "Downloadng the NeoVim conf from gitlab. \n"
    cd ~/.config || return
    git clone https://gitlab.com/sum4n/nvim.git
    break
  elif [[ $nvim == "n" ]]; then
    break
  else
    printf "WRONG: choose [y] or [n] \n"
  fi
done
sleep 2

# ----------------------------------- #
# ---- Sublime Text installation ----
# ----------------------------------- #
# I like sublime text so I install it every time
while true
do
  read -rp 'Want to install sublime [y/n]? ' sublime
  if [[ "$sublime" = "y" ]]; then
    if [ "$distro" = "d" ] || [ "$distro" = "debian" ]; then
      wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
      echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
      sudo apt-get update
      sudo apt-get install sublime-text
      break
    elif [ "$distro" = "a" ] || [ "$distro" = "arch" ]; then
      curl -O https://download.sublimetext.com/sublimehq-pub.gpg && sudo pacman-key --add sublimehq-pub.gpg && sudo pacman-key --lsign-key 8A8F901A && rm sublimehq-pub.gpg
      echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf
      sudo pacman -Syu sublime-text
      break
    fi
  elif [[ "$sublime" = "n" ]]; then
    printf "Next...\n"
    break
  else
    printf "select the wirght option!\n"
  fi
done

# ------------------------------ #
# ---- Install new crontabs ----
# ------------------------------ #
cd "$HOME" && crontab -l > newcron
echo "*/5 * * * * ~/.local/bin/batterynotify" >> newcron
echo "@weekly ~/bin/arch_cache_cleaner" >> newcron
crontab newcron
rm newcron

# -------------------------------- #
# ---- select your user shell ----
# -------------------------------- #
printf "Choose your liked shell. Either it's going to set a default shell which is (ZSH). \n"
sleep 2
while true
do
  read -rp 'Installed shells are [bash, zsh, fish] what you prefer? ' shell
  if [[ $shell == "bash" ]]; then
    sudo chsh "$USER" -s /bin/bash && printf "shell changed to BASH\n"
    break
  elif [[ $shell == "fish" ]]; then
    sudo chsh "$USER" -s /bin/fish && printf "shell changed to FISH\n"
    break
  elif [[ $shell == "zsh" ]]; then
    sudo chsh "$USER" -s /bin/zsh && printf "shell changed to ZSH\n"
    break
  elif [[ $shell == "" ]]; then
    sudo chsh "$USER" -s /bin/zsh && printf "shell changed to ZSH\n"
    break
  else
    printf "WRONG: select the wright shell!!\n"
  fi
done

# ------------------------- #
# ---- choose terminal ----
# ------------------------- #
printf "Choose your prefered terminal. Or it is set to default terminal (Alacritty).\n Main two terminal's are [ Alacritty && Kitty ]\n" && sleep 4
while true; do
  read -rp 'Choose your default terminal - (ala)critty || (kit)ty ' terminal
  if [ "$terminal" = "ala" ]; then
    echo alacritty > "$HOME"/SEDO/config/bspwm/term.cfg
    break
  elif [ "$terminal" = "kit" ]; then
    echo kitty > "$HOME"/SEDO/config/bspwm/term.cfg
    break
  elif [ "$terminal" = "" ]; then
    echo alacritty > "$HOME"/SEDO/config/bspwm/term.cfg
    break
  else
    printf "Wrong: choose wright option [ ala || kit ]!!\n"
  fi
done

# -------------------------------------------------------------------------- #
# ---- For notify user about to selfrun command after the script finish ----
# -------------------------------------------------------------------------- #
sleep 1
printf "remember to run xrdb ~/.Xresources. \n"
printf "END OF SCRIPT...\n Keep smile with SEDO.\n"
sleep 3

