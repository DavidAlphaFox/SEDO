#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

source ~/.config/zsh/alias
source ~/.config/zsh/env_value

eval "$(starship init bash)"

