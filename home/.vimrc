""" ---- options ----
set number
set relativenumber
set mouse=a
set clipboard="unnamedplus"
set termguicolors
set tabstop=2
set shiftwidth=2
set nowrap
set cursorline
set scrolloff=8
set signcolumn=auto
set ignorecase
set smartcase
set splitright
set noswapfile
set undofile
set incsearch
set nohlsearch
set colorcolumn=90
set wildmenu
set sidescrolloff=15
set spell
set spelllang=en_us

" For Autocomplete
set complete+=kspell
set completeopt=menuone,longest
set shortmess+=c

" startup nerdtree
" autocmd VimEnter *
"     \ NERDTreeToggle |

""" ---- plugins ----

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin()

Plug 'https://github.com/tpope/vim-commentary' " For Commenting gcc & gc
Plug 'https://github.com/sainnhe/sonokai' " colorscheme
Plug 'https://github.com/preservim/nerdtree' " NerdTree
Plug 'https://github.com/ryanoasis/vim-devicons' " Developer Icons
Plug 'https://github.com/vim-scripts/AutoComplPop' " Autocomplete popup

call plug#end()

""" ---- keymaps ----
" map leader key to spacebar
let mapleader = " "

" use [ii] for escape
inoremap ii <ESC>

" x is not going to copy
nnoremap x "_x

" file explorer
" nnoremap <leader>e <cmd>Lexplore<CR>
nnoremap <leader>e <cmd>NERDTreeToggle<CR>

" tab management
nnoremap <leader>to <cmd>tabnew<cr>
nnoremap <leader>tx <cmd>tabclose<cr>
nnoremap <tab> <cmd>tabn<cr>
nnoremap <S-tab> <cmd>tabp<cr>

" split window
nnoremap <leader>sv <C-w>v<cr>
nnoremap <leader>sh <C-w>h<cr>
nnoremap <leader>sx <cmd>close<cr>

" write and quit
nnoremap <leader>w <cmd>w<cr>
nnoremap <leader>q <cmd>q!<cr>
nnoremap <leader>z <cmd>wq<cr>

" shorter the navigation keys
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

""" ---- colorscheme ----
syntax on
if has('termguicolors')
  set termguicolors
endif

let g:sonokai_style = 'default'
let g:sonokai_better_performance = 1

colorscheme sonokai

