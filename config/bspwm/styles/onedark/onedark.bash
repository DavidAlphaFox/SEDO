
# Author 	 -  sum4n
# Source 	 -  https://gitlab.com/sum4n/SEDO
# Maintainer -  to_suman@outlook.com


foreground='#dfdfdf'
background='#282c34'
cursor='#5294e2'
color0='#32363D'
color1='#E06B74'
color2='#98C379'
color3='#E5C07A'
color4='#62AEEF'
color5='#C778DD'
color6='#55B6C2'
color7='#ABB2BF'
color8='#50545B'
color9='#EA757E'
color10='#A2CD83'
color11='#EFCA84'
color12='#6CB8F9'
color13='#D282E7'
color14='#5FC0CC'
color15='#B5BCC9'
