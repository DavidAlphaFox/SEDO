configuration {
    show-icons:                     true;
    display-drun: 					"";
    drun-display-format:            "{icon} {name}";
    disable-history:                false;
	click-to-exit: 					true;
    location:                       6;
}

@import "font.rasi"
@import "colors.rasi"

window {
    transparency:                   "real";
    background-color:               @BG;
    text-color:                     @FG;
    border:                  		2px;
    border-color:                  	@BGA;
    border-radius:                  8px;
    width:                          680px;
    x-offset:                       0;
    y-offset:                       -150px;
}

prompt {
    enabled: 						true;
	padding: 						0px;
	background-color: 				@BG;
	text-color: 					@FG;
}

textbox-prompt-colon {
	expand: 						false;
	str: 							"";
    background-color:               @BG;
    text-color:                     @IMG;
    padding:                        10px 10px 0px 10px;
    border:   2px 0px 2px 2px;
    border-color:   @SEL;
    border-radius:    4px;
	font:							"Font Awesome 6 Pro Solid 18";
}

entry {
    background-color:               @BG;
    text-color:                     @FG;
    placeholder-color:              @FG;
    expand:                         true;
    horizontal-align:               0;
    placeholder:                    "Type here to search";
    blink:                          true;
    border:                  		2px 2px 2px 0px;
    border-color:                  	@SEL;
    border-radius:                  4px;
    padding:                        10px;
}

inputbar {
	children: 						[ textbox-prompt-colon, entry ];
    spacing:                        0px;
    background-color:               @BGA;
    text-color:                     @FG;
    expand:                         false;
    border:                  		0px 0px 0px 0px;
    border-radius:                  4px;
    border-color:                  	@SEL;
    margin:                         0px 0px 0px 0px;
    padding:                        0px;
    position:                       center;
}

case-indicator {
    background-color:               @BG;
    text-color:                     @FG;
    spacing:                        0;
}


listview {
    background-color:               #00000000;
    columns:                        6;
    lines:							6;
    spacing:                        4px;
    cycle:                          false;
    dynamic:                        true;
    layout:                         vertical;
}

mainbox {
    background-color:               @BG;
    children:                       [ inputbar, listview ];
    spacing:                       	40px;
    padding:                        35px;
}

element {
    background-color:               #00000000;
    text-color:                     @FG;
    orientation:                    vertical;
    border-radius:                  4px;
    padding:                        20px;
}

element-icon {
    background-color: 				inherit;
    text-color:       				inherit;
    horizontal-align:               0.5;
    vertical-align:                 0.5;
    size:                           40px;
    border:                         0px;
}

element-text {
    background-color: 				inherit;
    text-color:       				inherit;
	font:							"Noto Sans 8";
    expand:                         true;
    horizontal-align:               0.5;
    vertical-align:                 0.5;
    margin:                         0px;
}

element normal.urgent,
element alternate.urgent {
    background-color:               @UGT;
    text-color:                     @FG;
    border-radius:                  9px;
}

element normal.active,
element alternate.active {
    background-color:               #343F44;
    text-color:                     @FG;
}

element selected {
    background-color:               #343F44;
    text-color:                     @FG;
    border:                  		2px 2px 2px 2px;
    border-radius:                  4px;
    border-color:                  	@BDR;
}

element selected.urgent {
    background-color:               @UGT;
    text-color:                     @FG;
}

element selected.active {
    background-color:               @BGA;
    color:                          @FG;
}
