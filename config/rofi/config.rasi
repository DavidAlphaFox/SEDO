
/* # Author 	 -  sum4n */
/* # Source 	 -  https://gitlab.com/sum4n/SEDO */
/* # Maintainer -  to_suman@outlook.com */

configuration {
    show-icons:                 true;
    display-drun:               "drun:";
    display-run:                "run:";
    display-filebrowser:        "fileBrowse:";
    display-window:             "windows:";
  	window-format:              "{w} · {c} · {t}";
}

@import                          "themes/colors.rasi"

* {
    height:                     36;
    background-color:           @BG;
    border-color:               @BDR;
    text-color:                 @foreground;
    font:                       "CaskaydiaCove Nerd Font Bold 16";

    border-colour:               var(selected);
    handle-colour:               var(selected);
    background-colour:           var(background);
    foreground-colour:           var(foreground);
    alternate-background:        var(background-alt);
    normal-background:           var(background);
    normal-foreground:           var(foreground);
    urgent-background:           var(urgent);
    urgent-foreground:           var(background);
    active-background:           var(active);
    active-foreground:           var(background);
    selected-normal-background:  var(selected);
    selected-normal-foreground:  var(background);
    selected-urgent-background:  var(active);
    selected-urgent-foreground:  var(background);
    selected-active-background:  var(urgent);
    selected-active-foreground:  var(background);
    alternate-normal-background: var(background);
    alternate-normal-foreground: var(foreground);
    alternate-urgent-background: var(urgent);
    alternate-urgent-foreground: var(background);
    alternate-active-background: var(active);
    alternate-active-foreground: var(background);
}

window {
    location:                    north;
    anchor:                      north;
    width:                       100%;
    padding:                     2px;
    border:                      0px solid;
    children: [ horibox ];
}

horibox {
    orientation: horizontal;
    children: [ prompt, entry, listview ];
}

prompt {
    margin:           2px;
    padding:          2px;
    background-color: @on;
    text-color:       @BG;
}

listview {
    lines:                       100;
    layout:                      horizontal;
    border:                      0px solid;
}

entry {
    padding: 6px;
    expand: false;
    width: 10em;
}

element {
    enabled:                     true;
    spacing:                     8px;
    margin:                      0px;
    padding:                     2px 8px;
    border:                      0px solid;
    cursor:                      pointer;
}
element normal.normal {
    background-color:            var(normal-background);
    text-color:                  var(normal-foreground);
}
element normal.urgent {
    background-color:            var(urgent-background);
    text-color:                  var(urgent-foreground);
}
element normal.active {
    background-color:            var(active-background);
    text-color:                  var(active-foreground);
}
element selected.normal {
    background-color:            @BG;
    text-color:                  @selected;
    border:                      0px 0px 3px 0px;
    border-radius:               0px;
    border-color:                @selected;
}
element selected.urgent {
    background-color:            var(selected-urgent-background);
    text-color:                  var(selected-urgent-foreground);
}
element selected.active {
    background-color:            var(selected-active-background);
    text-color:                  var(selected-active-foreground);
}
element alternate.normal {
    background-color:            var(alternate-normal-background);
    text-color:                  var(alternate-normal-foreground);
}
element alternate.urgent {
    background-color:            var(alternate-urgent-background);
    text-color:                  var(alternate-urgent-foreground);
}
element alternate.active {
    background-color:            var(alternate-active-background);
    text-color:                  var(alternate-active-foreground);
}
element-icon {
    background-color:            transparent;
    text-color:                  inherit;
    size:                        26px;
    cursor:                      inherit;
}
element-text {
    background-color:            transparent;
    text-color:                  inherit;
    highlight:                   inherit;
    cursor:                      inherit;
    vertical-align:              0.5;
    horizontal-align:            0.0;
}

message {
    enabled:                     true;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px 0px 0px 0px;
    border-color:                @border-colour;
    background-color:            transparent;
    text-color:                  @foreground-colour;
}

textbox {
    padding:                     10px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            @alternate-background;
    text-color:                  @foreground-colour;
    vertical-align:              0.5;
    horizontal-align:            0.0;
    highlight:                   none;
    placeholder-color:           @foreground-colour;
    blink:                       true;
    markup:                      true;
}

error-message {
    padding:                     10px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border-colour;
    background-color:            @background-colour;
    text-color:                  @foreground-colour;
}
