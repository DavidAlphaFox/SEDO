#!/bin/sh

# Author 	 -  sum4n
# Source 	 -  https://gitlab.com/sum4n/SEDO
# Maintainer -  to_suman@outlook.com
# Main Source - https://github.com/ChristianChiarulli \\ Christian Chiarulli

#----#----#----#
# Function to source files if they exist
function zsh_add_file() {
    [ -f "$ZDOTDIR/$1" ] && source "$ZDOTDIR/$1"
}

#----#----#----#
# Plugin manager
if test ! -d $HOME/git_files; then
  mkdir $HOME/git_files
  test ! -d $HOME/git_files/zsh-plug && mkdir $HOME/git_files/zsh-plug
fi
PLUGIN_DIR="$HOME/git_files"

function add_plug() {
    [ -f "$PLUGIN_DIR/$1" ] && source "$PLUGIN_DIR/$1"
}

function zsh_add_plugin() {
    PLUGIN_NAME=$(echo $1 | cut -d "/" -f 2)
    if [ -d "$PLUGIN_DIR/zsh-plug/$PLUGIN_NAME" ]; then 
        # For plugins
        add_plug "zsh-plug/$PLUGIN_NAME/$PLUGIN_NAME.plugin.zsh" || \
        add_plug "zsh-plug/$PLUGIN_NAME/$PLUGIN_NAME.zsh"
    else
        git clone "https://github.com/$1.git" "$PLUGIN_DIR/zsh-plug/$PLUGIN_NAME"
    fi
}

#----#----#----#
# For completions
function zsh_add_completion() {
    PLUGIN_NAME=$(echo $1 | cut -d "/" -f 2)
    if [ -d "$PLUGIN_DIR/zsh-plug/$PLUGIN_NAME" ]; then 
        # For completions
		completion_file_path=$(ls $PLUGIN_DIR/zsh-plug/$PLUGIN_NAME/_*)
		fpath+="$(dirname "${completion_file_path}")"
        add_plug "zsh-plug/$PLUGIN_NAME/$PLUGIN_NAME.plugin.zsh"
    else
        git clone "https://github.com/$1.git" "$PLUGIN_DIR/zsh-plug/$PLUGIN_NAME"
		fpath+=$(ls $PLUGIN_DIR/zsh-plug/$PLUGIN_NAME/_*)
		rm $ZDOTDIR/.zccompdump
    fi
	completion_file="$(basename "${completion_file_path}")"
	if [ "$2" = true ] && compinit "${completion_file:1}"
}
