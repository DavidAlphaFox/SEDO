# ARCH INSTALLATION INSTRUCTION. It's for GPT and EFI system.

## For First installation
* Edit the `/etc/pacman.conf` file uncomment the `ParallelDownloads` option.
* Edit the `/etc/pacman.d/mirrorlist` choose your Country mirrors.

## Choose your Country mirrors
```
pacman -S reflector
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector --protocol https --verbose --country '<your-country-name>' -l 8 --sort rate --save /etc/pacman.d/mirrorlist
```

### First set your keymaps
```
ls /usr/share/kbd/keymaps/**/*.map.gz | less <for list the available kemap>
loadkeys <your keymap>
```

### System clock
```
timedatectl set-ntp true
timedatectl status
```

### if you are using wifi connection then follow the commands.

```
iwctl
device list
station <device> scan
station <device> get-networks
station <device> connect <your_SSID>

```

### PARTITION YOUR DISK USING [CFDISK] IT'S MORE EASIER THAN THE OTHERS --- it's mandatory.

```
fdisk -l
cfdisk /path/to/directory

```
### Make your filesystem.

* **For efi part:**
```
mkfs.fat -F32 /path/to/EFI
```

* **For swap:**
```
mkswap /path/to/swap
swapon /path/to/swap
```
* **For root:**
```
mkfs.ext4 /path/to/root
```

### if you created home partition than.
```
mkfs.ext4 /path/to/home
```

### Mount your desired patition,s.
```
mount /path/to/root /mnt
mkdir /mnt/home
mount /path/to/home /mnt/home
```

### Install system
```
pacstrap -i /mnt base linux linux-firmware sudo vim
```

### Generate fstab file
```
genfstab -U /mnt >> /mnt/etc/fstab
```

### CHROOT into system
```
arch-chroot /mnt
```

### Set TimeZone
```
ln -sf /usr/share/zoneinfo/YourRegion/YourCity /etc/localtime
hwclock --systohc --utc
```

### Generate your locale.
```
vim /etc/locale.gen <uncomment your locale>
locale-gen
```

### Edit your HostName and Hosts.
```
touch /etc/hostname
echo <yourHostName> > /etc/hostname
vim /etc/hosts
```
### ⚠ Add this lines to your hosts file
```
127.0.0.1   localhost
::1         localhost
127.0.1.1   <hostname>.localdomain  <hostname>
```

### Create user and password
```
passwd
useradd -m <username>
passwd <username>
usermod -aG wheel,audio,video,optical,storage <username>
```

### Edit sudoers file
```
EDITOR=vim visudo <uncomment wheel related line>
```

### Install Grub
```
pacman -S grub efibootmgr dosfstools os-prober mtools
```

### EFI mount
```
mkdir /boot/EFI
mount /dev/(efi.part) /boot/EFI
```

### Final grub install
```
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
grub-mkconfig -o /boot/grub/grub.cfg
```

### Install your Needed Softwares. Important one
```
pacman -S networkmanager git curl wget bluez bluez-utils blueman
```

### Enable NetworkManager
```
systemctl enable NetworkManager.service
```

### Exit from chroot
```
exit
umount -l /mnt
```
<!-- ecUQ2c2G-GyZmKuAu5 -->
