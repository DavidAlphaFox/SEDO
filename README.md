# SEDO-BSPWM

## GruvBox-Material
![](assets/glance/gruvbox.png)
## EverForst
![](assets/glance/everforest.png)
## Flowers
![](assets/glance/flowers.png)
## Dracula
![](assets/glance/dracula.png)
## OneDark
![](assets/glance/one_dark.png)
## TokyoNight
![](assets/glance/tokyo_night.png)

### Rofi
![](assets/glance/rofi.png)
### PowerMenu
![](assets/glance/power_menu.png)
### Themes Available
![](assets/glance/themes.png)


### 💠 Installation:

This installer can download and setup BSPWM window manager in vanilla [DEBIAN] or [ARCH] based machines.
No need any desktop environment for setup. It downloads all it's needed packages and setup.

<b>In your terminal or in console put this commands</b>
- **First download the installer**
```sh
wget https://gitlab.com/sum4n/SEDO/-/raw/main/sedo_installer
```
- **Now give it execute permissions**
```sh
chmod +x sedo_installer
```
- **Finally run the installer**
```sh
./sedo_installer
```

* **Suman Kumar Das**
* **https://www.gitlab.com/sum4n -- GitLab**
* **https://www.github.com/Suman-1410 -- GitHub**



